%global fontname google-noto-cjk
%global fontconf google-noto

%global commit0 32a5844539f2e348ed36b44e990f9b06d7fb89fe
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

%global common_desc \
Noto Sans CJK comprehensively cover Simplified Chinese,Traditional Chinese, \
Japanese, and Korean in a unified font family. This includes the full coverage of CJK Ideographs \
with variation support for 4 regions, Kangxi radicals, Japanese Kana, Korean Hangul, and other CJK \
symbols and letters in the Basic Multilingual Plane of Unicode. \
%{nil}

Name:           google-noto-cjk-fonts
Version:        20170602
Release:        9
Summary:        Google Noto Sans CJK Fonts
License:        OFL
URL:            https://github.com/googlei18n/noto-cjk
Source0:        https://github.com/googlei18n/noto-cjk/archive/%{commit0}.tar.gz#/noto-cjk-%{shortcommit0}.tar.gz
Source1:        genfontconf.py
Source2:        genfontconf.sh

BuildArch:      noarch
BuildRequires:  fontpackages-devel python3 /usr/bin/xmllint
Requires:       fontpackages-filesystem google-noto-sans-cjk-ttc-fonts google-noto-serif-cjk-ttc-fonts

Obsoletes:      google-noto-cjk-fonts-common
Provides:       google-noto-cjk-fonts-common

%description
%common_desc

%define notocjkpkg(n:f:p:) \
%define _font_pkg_name() %1 \
%define subpkgname %{-n:%{-n*}} \
%define fontfiles %{-f:%{-f*}}\
%define fconf %{-p*}%{!-p:66}-%{fontconf}-%{subpkgname}.conf\
%package -n     google-noto-%subpkgname-fonts \
Summary:        %* font files for %{name} \
Requires:       %{name}-common = %{version}-%{release} \
\
%description -n google-noto-%subpkgname-fonts \
%common_desc \
\
The google-noto-%subpkgname-fonts package contains %* fonts. \
\
%_font_pkg -n google-noto-%subpkgname-fonts -f %{fconf} %fontfiles \
%{nil}

%notocjkpkg -n sans-cjk-ttc -f NotoSansCJK-*.ttc -p 65-0 Sans OTC

%notocjkpkg -n serif-cjk-ttc -f NotoSerifCJK-*.ttc -p 65-0 Serif OTC

%notocjkpkg -n sans-cjk-jp -f NotoSansCJKjp-*.otf Japanese Multilingual Sans OTF

%notocjkpkg -n serif-cjk-jp -f NotoSerifCJKjp-*.otf Japanese Multilingual Serif OTF

%notocjkpkg -n sans-mono-cjk-jp -f NotoSansMonoCJKjp-*.otf Japanese Multilingual Sans Mono OTF

%notocjkpkg -n sans-cjk-kr -f NotoSansCJKkr-*.otf Korean Multilingual Sans OTF

%notocjkpkg -n serif-cjk-kr -f NotoSerifCJKkr-*.otf Korean Multilingual Serif OTF

%notocjkpkg -n sans-mono-cjk-kr -f NotoSansMonoCJKkr-*.otf Korean Multilingual Sans Mono OTF

%notocjkpkg -n sans-cjk-sc -f NotoSansCJKsc-*.otf Simplified Chinese Multilingual Sans OTF

%notocjkpkg -n serif-cjk-sc -f NotoSerifCJKsc-*.otf Simplified Chinese Multilingual Serif OTF

%notocjkpkg -n sans-mono-cjk-sc -f NotoSansMonoCJKsc-*.otf Simplified Chinese Multilingual Sans Mono OTF

%notocjkpkg -n sans-cjk-tc -f NotoSansCJKtc-*.otf Traditional Chinese Multilingual Sans OTF

%notocjkpkg -n serif-cjk-tc -f NotoSerifCJKtc-*.otf Traditional Chinese Multilingual Serif OTF

%notocjkpkg -n sans-mono-cjk-tc -f NotoSansMonoCJKtc-*.otf Traditional Chinese Multilingual Sans Mono OTF

%notocjkpkg -n sans-jp -f NotoSansJP-*.otf Japanese Region-specific Sans OTF

%notocjkpkg -n serif-jp -f NotoSerifJP-*.otf Japanese Region-specific Serif OTF

%notocjkpkg -n sans-kr -f NotoSansKR-*.otf Korean Region-specific Sans OTF

%notocjkpkg -n serif-kr -f NotoSerifKR-*.otf Korean Region-specific Serif OTF

%notocjkpkg -n sans-sc -f NotoSansSC-*.otf Simplified Chinese Region-specific Sans OTF

%notocjkpkg -n serif-sc -f NotoSerifSC-*.otf Simplified Chinese Region-specific Serif OTF

%notocjkpkg -n sans-tc -f NotoSansTC-*.otf Traditional Chinese Region-specific Sans OTF

%notocjkpkg -n serif-tc -f NotoSerifTC-*.otf Traditional Chinese Region-specific Serif OTF

%package_help

%prep
%autosetup -n noto-cjk-%{commit0}
cp -p %{SOURCE1} %{SOURCE2} .
bash -x ./genfontconf.sh

%build

%install
install -m 0755 -d %{buildroot}%{_fontdir}

install -m 0644 -p NotoSansCJK-*.ttc %{buildroot}%{_fontdir}
install -m 0644 -p NotoSerifCJK-*.ttc %{buildroot}%{_fontdir}

install -m 0644 -p NotoSansCJK{jp,kr,sc,tc}-*.otf %{buildroot}%{_fontdir}
install -m 0644 -p NotoSerifCJK{jp,kr,sc,tc}-*.otf %{buildroot}%{_fontdir}
install -m 0644 -p NotoSansMonoCJK{jp,kr,sc,tc}-*.otf %{buildroot}%{_fontdir}

install -m 0644 -p NotoSans{JP,KR,SC,TC}-*.otf %{buildroot}%{_fontdir}
install -m 0644 -p NotoSerif{JP,KR,SC,TC}-*.otf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
            %{buildroot}%{_fontconfig_confdir}

for f in sans-cjk-ttc serif-cjk-ttc \
    sans-cjk-jp serif-cjk-jp sans-mono-cjk-jp \
    sans-cjk-kr serif-cjk-kr sans-mono-cjk-kr \
    sans-cjk-sc serif-cjk-sc sans-mono-cjk-sc \
    sans-cjk-tc serif-cjk-tc sans-mono-cjk-tc \
    sans-jp serif-jp \
    sans-kr serif-kr \
    sans-sc serif-sc \
    sans-tc serif-tc;
do
    fconf=$(basename -a *-%{fontconf}-$f.conf)
    if [ "$(echo $fconf | wc -w)" -ne 1 ]; then
       echo "Did not find unique \*-%{fontconf}-$f.conf file"
       exit 1
    fi

    install -m 0644 -p ${fconf} \
                %{buildroot}%{_fontconfig_templatedir}/${fconf}

    ln -s %{_fontconfig_templatedir}/${fconf} \
         %{buildroot}%{_fontconfig_confdir}/${fconf}
done

%files
%defattr(-,root,root)
%license LICENSE

%files help
%defattr(-,root,root)
%doc NEWS HISTORY README.formats README.third_party

%changelog
* Sat Oct 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 20170602-9
- Package init

